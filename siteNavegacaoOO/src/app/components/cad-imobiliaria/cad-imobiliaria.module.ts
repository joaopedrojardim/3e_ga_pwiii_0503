import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImobiliariaRoutingModule } from './cad-imobiliaria-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadImobiliariaRoutingModule
  ]
})
export class CadImobiliariaModule { }
